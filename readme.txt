=== Learn Gutenberg Example 4: Pre-Existing Shortcode ===
Contributors: rzen
Donate link: http://wordpressfoundation.org/donate/
Tags: gutenberg
Requires at least: 4.4
Tested up to: 4.9.4
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A sample Gutenberg Block to to provide new UI for a pre-existing shortcode.

== Description ==

This plugin is designed to serve as a demonstration for creating a custom Gutenberg block.

== Installation ==

1. Upload this plugin's directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Insert the "Notice Message" blog via the Gutenberg editor.


== Changelog ==

= 0.1.0 =
* Initial release of this demonstration code.
